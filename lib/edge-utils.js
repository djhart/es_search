
// Take the command line arguments and put them in an object
exports.parseArgs = function parseArgs(args){
    var conf = {};
    for ( var i in args){
        var option = JSON.parse(args[i]);
        _.extend(conf,option);
    }
    return conf;
};

exports.getES = function getES(conf) {

    var finalObject = {};
    var finalFields = {};

    var raw_results = edge.httpPost(conf.es_URL, conf.data);
    var es_data = JSON.parse(raw_results.data);
    var es_records = es_data.hits.hits;
    var fields = conf.fields;

    for (var i = 0; i < es_records.length; i++) {
        var record = es_records[i]._source;
        var entries = [];
        var currentEntry = {};


        delete record.message;

        for (var field in fields) {

            if (_.isObject(record[fields[field]])){
                for ( var key in record[fields[field]]) {
                    var subField = fields[field] + "." + key
                    var value = record[fields[field]][key];

                    currentEntry[subField] = value;
                    finalObject[i] = currentEntry;
                    //finalFields[key] = subField;
                    finalFields[subField] = '';
                }
            } else {

                finalFields[fields[field]] = '';
                entries.push(record[fields[field]]);

                currentEntry[fields[field]] = record[fields[field]];
                finalObject[i] = currentEntry;
            }
        }
    }
    //print("DBG: hart:" + JSON.stringify(finalObject));
    //print("DBG: hart:" + JSON.stringify(finalFields));
    var donald = _.keys(finalFields);
    //print("donald:" + donald);

    processHeaders(donald);
    for ( var item in finalObject){
       //print("item:" + JSON.stringify(finalObject[item]));
       processKeys(donald, finalObject[item]);
    }
};

function processHeaders(keys){
    var entry = [];
    var num = keys.length;

    for ( var i = 0; i < num; i++){
        entry.push([keys[i]]);
    }
    print(entry.join([separator = delimter]));
}


function processKeys(keys, item){
    var entry = [];
    var num = keys.length;

    for ( var i = 0; i < num; i++){
        entry.push(item[keys[i]]);
    }
    print(entry.join([separator = delimter]));
}



/*
 fields is an array of top level fields to search for in the ElasticSearch results.  If the field is an object, it will be flattened.
 Example:  {"cpu":{"user":"percent"},{"system":"percent"}  will result in
 cpu.user.percent
 cpu.system.percent
 */
//TODO topbeat: make 'fields' for all 3 ( filesystem / process / system )
exports.getFields = function getFields(app){
    var fields;

    switch(app)
    {
        case "Apica":
        case "apica":
            fields = ["id", "guid", "name", "check_type", "check_type_name", "check_type_api", "enabled", "location", "country_code", "sla_percent_current_month", "timestamp_utc", "severity", "value", "unit", "target_sla", "check_symbol", "threshold_w", "threshold_w_dynamic", "threshold_e", "threshold_e_dynamic", "scheduled_inclusion", "scheduled_exclusion", "interval_seconds", "groupId", "groupName", "subGroupName", "subGroupId"];
            break;
        case "Topebeat":
        case "topbeat":
            fields = ["cpu","count","beat","load","swap","type","@timestamp"];
            break;
        case "Arcsight":
        case "arcsight":
            fields = ["adarcSightEventPath", "agt", "ahost", "aid", "art", "assetCriticality", "at", "atz", "av", "c6a4", "c6a4Label", "cat", "categoryBehavior", "categoryDeviceGroup", "categoryObject", "categoryOutcome", "categorySignificance", "cs2", "cs2Label", "description", "destinationAddress", "destinationAssetId", "destinationHost", "destinationZoneID", "destinationZoneURI", "deviceSeverity", "deviceZoneID", "deviceZoneURI", "dtz", "dvc", "dvchost", "eventDate", "eventId", "eventSecond", "host", "locality", "modelConfidence", "mrt", "priority", "relevance", "severity"];
            //fields = ["categoryBehavior","geoipSrc","severity"];

            break;

        default:
            fields = null;
    }
    //TODO handle null and error since it's required
    return fields;
};

exports.httpGet = function httpGet(theUrl){
    var con = new java.net.URL(theUrl).openConnection();
    con.requestMethod = "GET";

    return asResponse(con);
};

exports.httpPost = function httpPost(theUrl, data, contentType){
    contentType = contentType || "application/json";
    var con = new java.net.URL(theUrl).openConnection();

    con.requestMethod = "POST";
    con.setRequestProperty("Content-Type", contentType);

    con.doOutput=true;
    edge.write(con.outputStream, data);

    return edge.asResponse(con);
};

exports.asResponse = function asResponse(con){
    var d = edge.read(con.inputStream);

    return {data : d, statusCode : con.responseCode};
};

exports.write = function write(outputStream, data){
    var wr = new java.io.DataOutputStream(outputStream);
    wr.writeBytes(data);
    wr.flush();
    wr.close();
};

exports.read = function read(inputStream){
    var inReader = new java.io.BufferedReader(new java.io.InputStreamReader(inputStream));
    var inputLine;
    var response = new java.lang.StringBuffer();

    while ((inputLine = inReader.readLine()) != null) {
        response.append(inputLine);
    }
    inReader.close();
    return response.toString();
};
