#!/usr/bin/jjs
load('./lib/jvm-npm.js');
load('./lib/underscore.js');

var edge = require('./lib/edge-utils.js');

var conf = edge.parseArgs(arguments);


System = Java.type("java.lang.System");
System.setProperty("com.arcsight.coma.client.ws.baseURL", "https://sol-arcsight:8443/www/");

var service = new com.arcsight.product.core.service.v1.client.ws.LoginServiceClientFactory().createClient();
var authToken = service.login(null, "admin", "arcsight");
var serviceQ = new com.arcsight.product.manager.resource.service.v1.client.ws.QueryViewerServiceClientFactory().createClient();

var donald = 'Actor	Agent Severity	Agent Type	Attacker Address	Attacker Asset Name	Attacker Host Name	Attacker User Name	Attacker Zone Name	Attacker Zone URI	Attacker Zone	AttackerHost	AttackerPipeTargetUser	Category Behavior	Category Device Group	Category Object	Category Outcome	Category Significance	Category Technique	Change Source	Change	Connector Information	Count	Customer Name	Device Address	Device Custom Number1 Label	Device Custom String1	Device Custom String2	Device Custom String3	Device Custom String5	Device Custom String6	Device Direction	Device Event Category	Device Event Class ID	Device Host Name	Device Inbound Interface	Device Information	Device Outbound Interface	Device Product	Device Vendor	Device Zone Name	Device Zone URI	End Time	File Name	File Path	Generator URI	HHMM	HostInfo	Hour	Hour(End Time)	Internal Host Name	Internal Host	LoginHour	Name	Priority	Protocol	Request Url Host	Request Url	Target Address	Target Host Name	Target Mac Address	Target Port	Target User ID	Target User Name	Target Zone Name	Target Zone URI	User Name	User	Vendor and Product	VendorInformation';

// crude way to get DS to have all fields that may be sent from Arcsight
// for the first run of the DS, uncomment the following 2 lines and then
// comment them out again
//print(donald);
//exit();


try{
  //var md = serviceQ.getMatrixData(authToken, "cJ37PMj4BABCWXl3XQ0hdew==");
  var md = serviceQ.getMatrixData(authToken, conf.m_resourceId);
  var headers = md.getColumnHeaders();
  headers.forEach(showStuff);
  print();


  var rows = md.getRows();

  for (var i = 0; i< rows.length; i++) {
      cell = rows[i].getValue();
      cell.forEach(showStuff);
      print();
  }
} catch(e) {
  print("error?:" + e);
}


function showStuff(value){
    //print("value:" + value);
    java.lang.System.out.print(value + '\t');
}
