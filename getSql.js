#!/usr/bin/jjs
System = Java.type("java.lang.System");
Class = Java.type("java.lang.Class");

System.setProperty("com.arcsight.coma.client.ws.baseURL", "https://sol-arcsight:8443/www/");

var service = new com.arcsight.product.core.service.v1.client.ws.LoginServiceClientFactory().createClient();
var authToken = service.login(null, "admin", "arcsight");
//var serviceQ = new com.arcsight.product.manager.resource.service.v1.client.ws.QueryServiceClientFactory().createClient();
var serviceQ = new com.arcsight.product.manager.resource.service.v1.client.ws.QueryViewerServiceClientFactory().createClient();

var allIds = serviceQ.findAllIds(authToken);

//print("resourceId\tname\tdescription");
print("resourceId\tqueryName\tdescription");
for ( var i = 0; i<allIds.length; i++){
    var resource = serviceQ.findByUUID(authToken, allIds[i]);
    print(allIds[i] + '\t' + resource.name + '\t' + resource.description);
}
