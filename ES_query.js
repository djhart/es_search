#!/usr/bin/jjs
load('./lib/jvm-npm.js');
load('./lib/underscore.js');

//TODO add logging

var edge = require('./lib/edge-utils.js');

var conf = edge.parseArgs(arguments);

/*
 * Search the whole index or just a specific type? Final URL will look similar to one
 * of these:
 *
 * http://server:port/index/_search
 * or
 * http://server:port/index/type/_search
 */

if ( conf.es_type ){
    //print("we havea a type:" + conf.es_type);
    conf.es_URL = conf.es_host + '/' + conf.es_index + '/' + conf.type + '/_search';
} else {
    //print("no type defined");
    conf.es_URL = conf.es_host + '/' + conf.es_index + '/_search';
}


//TODO add more query options?
conf.data = '{"size":' + conf.size + '}, "query": { "match_all": {} } },{"sort": ["@timestamp": {"order": "desc", "unmapped_type": "boolean"}}]}';
conf.fields = edge.getFields(conf.es_app);


//TODO move this somewhere better
var delimter = '\t';
//var delimter = '|';


edge.getES(conf);
